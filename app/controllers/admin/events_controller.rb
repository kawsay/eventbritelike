class Admin::EventsController < AdminsController
  def index
    @events = Event.all.sort
  end

  def show
    @event = Event.find(params[:id])
    @users = User.all
  end
end