class AdminsController < ApplicationController
  before_action :check_if_admin
  def index
    @events = Event.all
  end

  private

  def check_if_admin
    if current_user == nil
      flash[:danger] = "You must login first"
      redirect_to :root
    else
      if current_user.is_admin == false
        flash[:danger] = "You have no access to this section"
        redirect_to :root
      end
    end
  end

end